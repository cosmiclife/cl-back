celery==5.0.5

Django==3.1.6
django-cors-headers==3.7.0
django-environ==0.4.5
django-fieldsignals==0.6.0
django-filter==2.4.0
django-guardian==2.3.0
django-redis==4.12.1
django-phonenumber-field==5.0.0
phonenumbers==8.12.18

drf-yasg==1.20.0
djangorestframework==3.12.2
djangorestframework-guardian==0.3.0
djangorestframework-recaptcha==0.2.0
djangorestframework-jwt==1.11.0
django-versatileimagefield==1.10

psycopg2-binary==2.8.6

oauth2client==4.1.3
django-oauth-toolkit==1.4.0
sentry-sdk==1.0.0
django-taggit==1.3.0
channels==3.0.5
channels-redis==3.4.1
django-admin-history==2.2.0
importlib-metadata==4.13.0
