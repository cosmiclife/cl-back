INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'corsheaders',
    'django_filters',
    'rest_framework',
    'rest_framework.authtoken',
    # 'rest_framework_swagger',
    'drf_yasg',
    'guardian',
    'phonenumber_field',

    'django_history',
    'apps.accounts',
    'apps.web',
    'apps.avatars',
    'oauth2_provider',
    'channels',

    'django.contrib.admin',
]
