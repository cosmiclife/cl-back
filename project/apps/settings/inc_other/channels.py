import os
from apps.settings.common import env
from apps.settings.common import SECRET_KEY

CHANNEL_REDIS_HOST = os.environ.get(env.DEFAULT_CACHE_ENV, 'redis://redis/10')

ASGI_APPLICATION = "apps.routing.application"
CHANNEL_LAYERS = {
    'default': {
        'BACKEND': 'channels_redis.core.RedisChannelLayer',
        'CONFIG': {
            "hosts": [CHANNEL_REDIS_HOST],
            "symmetric_encryption_keys": [SECRET_KEY],
        },
    },
}