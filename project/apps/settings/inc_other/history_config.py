OBSERVED_FIELDS = {
    'accounts.User':
        (
            'username', 'email', 'user_permissions',
        ),
    'avatars.Avatar':
        (
            'avatar_name', 'account', 'hero',
        ),
}
