from django.conf import settings
from django.contrib.auth import models as auth_models
from django.db import models
from django.utils.translation import gettext_lazy as _

from apps.api.v1.tokens import EmailVerifyTokenGenerator
from apps.helpers.models import CreatedDeletedModel
from .tasks import send_templated_email


class User(CreatedDeletedModel, auth_models.AbstractUser):
    email = models.EmailField(_('email адрес'), null=True, blank=False, unique=True, db_index=True)

    def __str__(self):
        return f"{self.username} ({self.email})"

    def get_model(self):
        return "user"

    def get_status(self):
        if self.deleted_at:
            return {'title': _("Удален"), 'color': 'error', 'id': 5}
        elif self.is_superuser:
            return {'title': _("Администратор"), 'color': 'accent', 'id': 4}
        elif self.is_staff:
            return {'title': _("Сотрудник"), 'color': 'warning', 'id': 3}
        elif self.is_active:
            return {'title': _("Активный"), 'color': 'success', 'id': 2}
        return {'title': _("Не активирован"), 'color': 'secondary', 'id': 1}

    def send_verify_email(self):
        key = EmailVerifyTokenGenerator().make_token(self)
        url = settings.REG_VERIFY_URL.format(self.pk, key)
        return send_templated_email.delay(
            _('Подтвердите Ваш электронный адрес'),
            self.email,
            'accounts/verify_email.txt',
            'accounts/verify_email.html',
            url=url, email=self.username, site_name=settings.REG_SITE_NAME
        )

    def send_change_password_email(self):
        password = User.objects.make_random_password()
        self.set_password(password)
        self.save()
        return send_templated_email.delay(
            _('Сброс пароля'),
            self.email,
            'accounts/change_password.txt',
            'accounts/change_password.html',
            password=password, email=self.username, site_name=settings.REG_SITE_NAME
        )

    class Meta(auth_models.AbstractUser.Meta):
        ordering = ('-created_at', 'pk')
        swappable = 'AUTH_USER_MODEL'
        verbose_name = _('пользователя')
        verbose_name_plural = _('Пользователи')
        indexes = (models.Index(fields=('username', 'email',)),)

