from channels.routing import ProtocolTypeRouter, URLRouter
from apps.avatars.routing import websockets

application = ProtocolTypeRouter({
    "websocket": websockets,
})

