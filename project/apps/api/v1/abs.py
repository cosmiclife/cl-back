from rest_framework import serializers


class HeadersSerializer(serializers.Serializer):
    text = serializers.CharField()
    value = serializers.CharField()
    active = serializers.BooleanField(required=False, default=True)
    align = serializers.ChoiceField(choices=['text-xs-right', 'text-xs-left', 'text-xs-center'], required=False)
    sortable = serializers.BooleanField(required=False, default=True)
    type = serializers.ChoiceField(
        choices=[
            'char', 'number', 'date', 'time', 'datetime', 'picture', 'none', 'static', 'dynamic', 'label', 'boolean',
            'active-boolean', 'email', 'phone', 'color', 'customtime', 'duration', 'file', 'simple-file', 'textarea'
        ],
        required=False
    )
    ref = serializers.ListField(required=False)
    readonly = serializers.BooleanField(required=False, default=False)
    import_data = serializers.BooleanField(required=False, default=False)
    export_data = serializers.BooleanField(required=False, default=False)
    required = serializers.BooleanField(required=False, default=True)
    filtered = serializers.BooleanField(required=False, default=False)
