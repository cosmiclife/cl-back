from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.utils.translation import gettext_lazy as _
from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, serializers, status, filters
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings

from apps.accounts import defaults
from apps.api.v1.permissions import ObjectPermissions
from apps.api.v1.serializers import BadRequestResponseSerializer, OkRequestResponseSerializer
from apps.api.v1.serializers.accounts import (
    UserFullSerializer, UserRegistrationSerializer, UserChangePasswordSerializer, UserHeadersSerializer,
    UserRestorePasswordSerializer, UserLoginSerializer
)
from apps.api.v1.tokens import EmailVerifyTokenGenerator
from apps.api.v1.viewsets import ExtendedModelViewSet, ReadOnlyExtendedModelViewSet, RUDExtendedModelViewSet

from guardian.shortcuts import get_objects_for_user

User = get_user_model()
jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER


class UserViewSet(ReadOnlyExtendedModelViewSet):
    """
    create:
    Добавление нового пользователя

    :return 201

    retrieve:
    Возвращает данные пользователя

    :param ID пользователя
    :return 200

    list:
    Выводит список всех пользователей

    :return 200

    update:
    Изменение данных пользователя

    :param ID пользователя
    :return 200

    partial_update:
    Частичное обновление пользовательских данных

    :param ID пользователя
    :return 200

    delete:
    Удаление пользователя

    :param ID пользователя
    :return 204
    """

    queryset = User.objects.exclude(username='AnonymousUser')
    serializer_class = UserFullSerializer
    serializer_class_map = {
        'registration': UserRegistrationSerializer,
        'change_password': UserChangePasswordSerializer,
        'headers': UserHeadersSerializer,
        'restore_password': UserRestorePasswordSerializer,
        'login': UserLoginSerializer,
    }
    permission_classes = (ObjectPermissions,)
    permission_map = {
        'registration': permissions.AllowAny,
        'verify_email': permissions.AllowAny,
        'restore_password': permissions.AllowAny,
        'login': permissions.AllowAny,
    }
    filter_backends = (filters.OrderingFilter, filters.SearchFilter,)
    ordering_fields = (
        'id', 'username', 'email', 'phone', 'gender', 'first_name', 'middle_name', 'last_name', 'birthday',
        'last_login', 'date_joined'
    )
    search_fields = ('$username', '$first_name', '$middle_name', '$last_name', '$email', '$phone')
    ordering = ('id',)

    def get_queryset(self):
        users = get_objects_for_user(self.request.user, 'accounts.view_user', accept_global_perms=False)
        return users.exclude(username='AnonymousUser')

    @action(methods=['post'], detail=False)
    def login(self, request):
        serializer = self.get_serializer(data=request.data, instance=request)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            try:
                avatar_id = request.user.avatar.all().first().avatar_id
            except Exception as e:
                avatar_id = ''
            return Response({'status': 'ok', 'avatar_id': avatar_id, 'account': True}, status=status.HTTP_200_OK)

    @swagger_auto_schema(responses={200: serializers.Serializer, 400: BadRequestResponseSerializer})
    @action(methods=['get'], detail=False)
    def me(self, request, pk=None, **kwargs):
        """
        Возвращает данные пользователя

        :param request: auth user
        :return 200
        """
        serializer = self.get_serializer(instance=request.user)
        return Response(serializer.data)

    @swagger_auto_schema(
        responses={
            200: UserChangePasswordSerializer,
            400: BadRequestResponseSerializer,
            410: BadRequestResponseSerializer,
        }
    )
    @action(methods=['post'], detail=False, url_path='change-password')
    def change_password(self, request):
        """
        Смена пароля пользователя

        :param request: password, password1, password2
        :return: 200
        """
        serializer = self.get_serializer(data=request.data, instance=request.user)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            data = UserFullSerializer(instance=request.user).data
            return Response(data)

    @swagger_auto_schema(responses={201: UserRegistrationSerializer, 400: BadRequestResponseSerializer})
    @action(methods=['post'], detail=False)
    def registration(self, request):
        """
        Регистрация пользователя через форму регистрации, с отправкой email уведомления

        :param: username, first_name, last_name, email, phone, password, passworddup.
        :return 201
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            user = serializer.save()
            data = UserFullSerializer(instance=user).data
            return Response(data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(responses={200: serializers.Serializer, 400: BadRequestResponseSerializer})
    @action(methods=['get'], detail=True, url_path=r'verify-email/(?P<key>[^/.]+)')
    def verify_email(self, request, pk=None, key=None, **kwargs):
        """
        Подтверждение адреса электронной почты

        :param: id-идентификатор пользователя, key - Уникальный ключ
        :return 200
        """
        user = get_object_or_404(User, pk=pk)
        is_valid_token = EmailVerifyTokenGenerator().check_token(user, key)
        if is_valid_token:
            user.is_active = True
            user.save()
            return Response({'status': 'success'}, status=status.HTTP_200_OK)
        else:
            return Response({'errors': {'token': _('Not valid email token')}}, status=status.HTTP_400_BAD_REQUEST)

    def _gender(self):
        genders = [{"value": k, "text": v} for k, v in defaults.GENDER_CHOICES]
        return genders

    @swagger_auto_schema(
        responses={
            200: UserHeadersSerializer,
            400: BadRequestResponseSerializer,
        }
    )
    @action(methods=['get'], detail=False)
    def headers(self, request, pk=None, key=None, **kwargs):
        """
        Название полей таблицы, формирует в ЛК отображаемые поля

        :return 200
        """
        data = [
            {'text': _('ID'), 'value': 'id', 'active': 'true', 'align': 'text-xs-right', 'sortable': 'true',
             'readonly': 'true', 'type': 'number'},
            {'text': _('Логин'), 'value': 'username', 'active': 'false', 'sortable': 'true', 'type': 'login'},
            {'text': _('E-mail'), 'value': 'email', 'active': 'true', 'sortable': 'true', 'type': 'email'},
            {'text': _('Дата авторизации'), 'value': 'last_login', 'active': 'false', 'align': 'text-xs-right',
             'type': 'datetime', 'readonly': 'true'},
            {'text': _('Дата создания'), 'value': 'date_joined', 'active': 'false', 'align': 'text-xs-right',
             'type': 'datetime', 'readonly': 'true'},
            {'text': _('Управление'), 'value': 'id', 'active': 'true', 'sortable': 'false'}
        ]
        serializer = self.get_serializer(data, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(responses={200: serializers.Serializer, 404: BadRequestResponseSerializer})
    @action(methods=['post'], detail=False, url_path=r'restore-password')
    def restore_password(self, request, pk=None, **kwargs):
        """
        Востановление пароля, небезопасный метод, пользователю приходит в письме его новый пароль

        :return 200
        """
        user = get_object_or_404(User, username=request.data['username'])
        serializer = self.get_serializer(data=request.data, instance=user)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return Response({'status': 'success'}, status=status.HTTP_200_OK)
