import uuid
import logging
import datetime
from decimal import Decimal
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.utils.translation import gettext_lazy as _
from drf_yasg.utils import swagger_auto_schema
from rest_framework import permissions, serializers, status, filters
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings

from apps.avatars import defaults, models
from apps.api.v1.permissions import ObjectPermissions
from apps.api.v1.serializers import BadRequestResponseSerializer, OkRequestResponseSerializer
from apps.api.v1.serializers.avatars import (
    AvatarFullSerializer, AvatarCreateSerializer, AvatarStartSerializer, HeroSerializer, VictorySerializer, CartSerializer
)
from apps.api.v1.tokens import EmailVerifyTokenGenerator
from apps.api.v1.viewsets import ExtendedModelViewSet, ReadOnlyExtendedModelViewSet, RUDExtendedModelViewSet
from apps.avatars.nickname import generate
from apps.avatars import utils
from guardian.shortcuts import get_objects_for_user

User = get_user_model()
jwt_response_payload_handler = api_settings.JWT_RESPONSE_PAYLOAD_HANDLER

logger = logging.getLogger(__name__)


class AvatarViewSet(ReadOnlyExtendedModelViewSet):
    """
        Аватар
    """

    queryset = models.Avatar.objects.select_related()
    serializer_class = AvatarFullSerializer
    serializer_class_map = {
        'start': AvatarStartSerializer,
        'hero_avatar': HeroSerializer,
        'victory': VictorySerializer,
        'cart': CartSerializer,
    }
    permission_classes = (ObjectPermissions,)

    @swagger_auto_schema(responses={200: serializers.Serializer, 400: BadRequestResponseSerializer})
    @action(methods=['get'], detail=False, url_path='me/(?P<avatar_id>[^/.]+)')
    def me(self, request, pk=None, avatar_id=None, **kwargs):
        """
        Отображение данных персонажа

        :param request:
        :return 200
        """
        avatar = get_object_or_404(models.Avatar, avatar_id=uuid.UUID(str(avatar_id)))
        data = AvatarFullSerializer(instance=avatar, context={'request': request}).data
        return Response(data)

    @swagger_auto_schema(responses={201: serializers.Serializer, 400: BadRequestResponseSerializer})
    @action(methods=['post'], detail=False)
    def start(self, request, pk=None, **kwargs):
        """
        Создание Аватара

        :param request:
        :return 201
        """
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            avatar_id = serializer.data.get('avatar_id')
            avatar = get_object_or_404(models.Avatar, avatar_id=uuid.UUID(str(avatar_id)))
            data = AvatarCreateSerializer(instance=avatar).data
            return Response(data)

    @swagger_auto_schema(responses={200: serializers.Serializer, 400: BadRequestResponseSerializer})
    @action(methods=['get'], detail=False, url_path='generate-name')
    def generate_name(self, request, pk=None, **kwargs):
        """
        Генерация имени Аватара

        :param request:
        :return 200
        """
        name = generate('ru')
        return Response({'avatar_name': name})

    @swagger_auto_schema(responses={200: serializers.Serializer, 400: BadRequestResponseSerializer})
    @action(methods=['get'], detail=False, url_path='hero-avatar')
    def hero_avatar(self, request, pk=None, **kwargs):
        """
        Список доступных аватарок

        :param request:
        :return 200
        """
        data = [
            {
                'id': hero.id,
                'name': hero.name,
                'image': hero.image
            } for hero in models.Hero.objects.select_related()
        ]
        serializer = self.get_serializer(data, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(responses={200: serializers.Serializer, 400: BadRequestResponseSerializer})
    @action(methods=['post'], detail=False, url_path='victory/(?P<avatar_id>[^/.]+)')
    def victory(self, request, pk=None, avatar_id=None, **kwargs):
        avatar = get_object_or_404(models.Avatar, avatar_id=uuid.UUID(avatar_id))
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            foe_pk = serializer.data.get("foe_pk", "")
            timer = serializer.data.get("timer", "")
            click = serializer.data.get("click", "")
            if not foe_pk:
                return Response({'status': 'fail'})
            try:
                foe = avatar.foes.get(consumer_pk=foe_pk, is_killed=False)
            except Exception as e:
                logger.info([f.consumer_pk for f in avatar.foes.all()])
                return Response({'status': 'fail', 'message': str(e)})
            if not avatar.is_victory(foe.life, click, timer):
                # foe.modified = datetime.datetime.now() # Делаем сброс времени модификации, что бы исключить отправку чуть позже
                # foe.save()
                return Response({'status': 'fail'})
            else:
                foe.life = 0
                foe.is_killed = True
                foe.level += 1
                foe.save()
                if foe.consumer.enemy_type == defaults.EC_BOSS:
                    level = avatar.level + 1
                    avatar.level = level
                    avatar.attack = avatar.get_attack()
                avatar.money += avatar.get_money()
                avatar.total_money += avatar.get_money()
                avatar.total_kill += 1
                avatar.is_bot = False
                avatar.save()
                avatar.set_log("Победа над врагом - %s" % foe, "Сражение")
                utils._get_enemy(avatar)
                utils._get_items(avatar)
                data = AvatarFullSerializer(instance=avatar, context={'request': request}).data
                return Response(data)
        return Response(serializer.data)

    @swagger_auto_schema(responses={200: serializers.Serializer, 400: BadRequestResponseSerializer})
    @action(methods=['post'], detail=False, url_path='cart/(?P<avatar_id>[^/.]+)')
    def cart(self, request, pk=None, avatar_id=None, **kwargs):
        """
        Покупка и продажа товара

        :param request: action: <buy|sell>
        :return 200
        """
        avatar = get_object_or_404(models.Avatar, avatar_id=uuid.UUID(str(avatar_id)))
        serializer = self.get_serializer(data=request.data, context={'request': request})
        if serializer.is_valid(raise_exception=True):
            item_pk = serializer.data.get("item_pk", "")
            action = serializer.data.get("action", "")
            if not item_pk or not action:
                return Response({'status': 'fail'})
            cart = avatar.carts.get(pk=item_pk)
            if action == 'buy' and avatar.money >= cart.get_price('buy'):
                avatar.money -= Decimal(cart.get_price('buy'))
                avatar.save()
                cart.quantity += 1
                cart.save()
                avatar.set_log("Покупка оружия - %s" % cart, "Товары")
            elif action == 'sell' and cart.quantity > 0:
                avatar.money += Decimal(cart.get_price('sell'))
                avatar.save()
                cart.quantity -= 1
                cart.save()
                avatar.set_log("Продажа оружия - %s" % cart, "Товары")
            else:
                return Response({'status': 'fail'})
            data = AvatarFullSerializer(instance=avatar, context={'request': request}).data
            return Response(data)
        return Response(serializer.data)