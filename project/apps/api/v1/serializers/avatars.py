import base64
import imghdr
import uuid
import random
import traceback

from django.contrib.auth import get_user_model
from django.contrib.auth.password_validation import validate_password
from django.core.files.base import ContentFile
from django.core.validators import MaxLengthValidator, MinLengthValidator
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers
from rest_framework.validators import UniqueValidator, ValidationError
from apps.avatars import defaults, models, utils
from apps.api.v1.abs import HeadersSerializer

User = get_user_model()

class FoeSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Foe
        fields = '__all__'


class AvatarCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Avatar
        fields = ('avatar_id',)


class AvatarFullSerializer(serializers.ModelSerializer):
    avatar = serializers.SerializerMethodField()
    #foes = FoeSerializer(many=True)
    foes = serializers.SerializerMethodField()
    items = serializers.SerializerMethodField()
    attack = serializers.SerializerMethodField()
    attack_sec = serializers.SerializerMethodField()
    money = serializers.SerializerMethodField()
    achievemets = serializers.SerializerMethodField()
    new_achievemets = serializers.SerializerMethodField()

    def get_achievemets(self, obj):
        return obj.get_achievement()

    def get_new_achievemets(self, obj):
        return obj.get_new_achievement()

    def get_money(self, obj):
        return obj.get_money_humanize()

    def get_attack_sec(self, obj):
        return float('{:.2f}'.format(obj.get_attack_sec()))

    def get_attack(self, obj):
        return float('{:.2f}'.format(obj.attack))

    def get_avatar(self, obj):
        request = self.context.get('request')
        if request:
            return str(request.build_absolute_uri(obj.hero.image.url))
        else:
            return str(obj.hero.image.url)

    def get_foes(self, obj):
        request = self.context.get('request')
        if request:
            return [{
                'id': foe.consumer.pk,
                'name': str(foe.consumer.name),
                'enemy_type_id': str(foe.consumer.enemy_type),
                'enemy_type_title': str(foe.consumer.get_enemy_type_display()),
                'image': str(request.build_absolute_uri(foe.consumer.image.url)),
                'life': foe.life
            } for foe in obj.foes.filter(is_killed=False)]
        else:
            return [{
                'id': foe.consumer_pk,
                'name': str(foe.consumer.name),
                'enemy_type_id': str(foe.consumer.enemy_type),
                'enemy_type_title': str(foe.consumer.get_enemy_type_display()),
                'image': str(foe.consumer.image.url),
                'life': foe.life
            } for foe in obj.foes.filter(is_killed=False)]

    def get_items(self, obj):
        request = self.context.get('request')
        if request:
            return [{
                'id': item.pk,
                'quantity': item.quantity,
                'title': str(item.consumer.title),
                'image': str(request.build_absolute_uri(item.consumer.image.url)),
                'price_buy': item.get_price_humanize('buy'),
                'price_sell': item.get_price_humanize('sell')

            } for item in obj.carts.select_related()]
        else:
            return [{
                'id': item.pk,
                'quantity': item.quantity,
                'title': str(item.consumer.title),
                'image': str(item.consumer.image.url),
                'price_buy': item.get_price_humanize('buy'),
                'price_sell': item.get_price_humanize('sell')

            } for item in obj.carts.select_related()]

    class Meta:
        model = models.Avatar
        fields = (
            'avatar_name', 'account', 'avatar', 'level', 'total_kill', 'attack', 'attack_sec', 'money', 'foes', 'items',
            'achievemets', 'new_achievemets'
        )


class HeroSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Hero
        fields = ('id', 'name', 'image')


class AvatarStartSerializer(serializers.ModelSerializer):
    def create(self, validated_data):

        ModelClass = self.Meta.model

        try:
            instance = ModelClass._default_manager.create(**validated_data)
        except TypeError:
            tb = traceback.format_exc()
            msg = (
                'Got a `TypeError` when calling `%s.%s.create()`. '
                'This may be because you have a writable field on the '
                'serializer class that is not a valid argument to '
                '`%s.%s.create()`. You may need to make the field '
                'read-only, or override the %s.create() method to handle '
                'this correctly.\nOriginal exception was:\n %s' %
                (
                    ModelClass.__name__,
                    ModelClass._default_manager.name,
                    ModelClass.__name__,
                    ModelClass._default_manager.name,
                    self.__class__.__name__,
                    tb
                )
            )
            raise TypeError(msg)
        utils._get_enemy(instance)
        utils._get_items(instance)
        return instance

    class Meta:
        model = models.Avatar
        fields = (
            'id', 'avatar_name', 'hero', 'avatar_id'
        )
        read_only_fields = (
            'id', 'avatar_id'
        )


class VictorySerializer(serializers.Serializer):
    foe_pk = serializers.IntegerField(required=True)
    timer = serializers.DecimalField(max_digits=19, decimal_places=2, required=True)
    click = serializers.IntegerField(required=True)


class CartSerializer(serializers.Serializer):
    item_pk = serializers.IntegerField(required=True)
    action = serializers.CharField()


class RankingSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Avatar
        fields = (
            'id', 'level', 'avatar_name'
        )
