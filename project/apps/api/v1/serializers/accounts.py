import base64
import imghdr
import uuid

from django.contrib.auth import authenticate, get_user_model, login
from django.contrib.auth.password_validation import validate_password
from django.core.files.base import ContentFile
from django.core.validators import MaxLengthValidator, MinLengthValidator
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers
from rest_framework.validators import UniqueValidator, ValidationError
from apps.accounts import defaults
from apps.api.v1.abs import HeadersSerializer

User = get_user_model()


class UserHeadersSerializer(HeadersSerializer):
    pass


class UserLoginSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def update(self, instance, validated_data):

        username = validated_data.get('username')
        password = validated_data.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(instance, user)
            else:
                raise ValidationError(_("Пользователь не активирован в системе"))
        else:
            raise ValidationError(_("Пользователь с таким логином и паролем не найден"))
        return validated_data


class UserFullSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(required=False, validators=[UniqueValidator(User.objects.all())])

    class Meta:
        model = User
        fields = (
            'id', 'username', 'email', 'last_login', 'date_joined'
        )
        read_only_fields = (
            'id', 'email', 'last_login', 'date_joined'
        )


class UserRegistrationSerializer(serializers.Serializer):
    username = serializers.CharField(validators=[MaxLengthValidator(150), UniqueValidator(User.objects.all())])
    email = serializers.EmailField(validators=[UniqueValidator(User.objects.all())])

    password = serializers.CharField()
    passworddup = serializers.CharField()

    def validate_passworddup(self, value):
        validate_password(value)
        return value

    def validate(self, attrs):
        if attrs['password'] != attrs['passworddup']:
            raise ValidationError(detail=_('Пароли не совпадают'))
        return super().validate(attrs)

    def create(self, validated_data):
        username = validated_data.get('username')
        user = User.objects.create(
            username=username,
            email=validated_data.get('email'),
            is_active=False,
        )
        user.set_password(validated_data.get('passworddup'))
        user.save()
        user.send_verify_email()
        return user


class UserChangePasswordSerializer(serializers.Serializer):
    password = serializers.CharField(required=True)
    password1 = serializers.CharField(required=True)
    password2 = serializers.CharField(required=True)

    def update(self, instance, validated_data):
        instance.set_password(validated_data['password2'])
        instance.change_password = timezone.now()
        instance.save()
        return self.instance

    def validate_password(self, value):
        if not self.instance.check_password(value):
            raise ValidationError(detail=_('Введенный пароль некорректен'))
        return value

    def validate_password1(self, value):
        validate_password(value)
        return value

    def validate_password2(self, value):
        validate_password(value)
        return value

    def validate(self, attrs):
        if attrs['password1'] != attrs['password2']:
            raise ValidationError(detail=_('Пароли не совпадают'))
        return super().validate(attrs)


class UserRestorePasswordSerializer(serializers.Serializer):
    username = serializers.CharField()

    def update(self, instance, validated_data):
        instance.change_password = None
        instance.save()
        instance.send_change_password_email()
        return self.instance
