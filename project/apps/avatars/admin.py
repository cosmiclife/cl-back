from django.contrib import admin
from django.db.models import ImageField
from apps.avatars import models, widgets
from django.db.models import JSONField
from apps.helpers.utils import ReadableJSONFormField
from django_history.admin import HistoryBlockAdmin


class CartInline(admin.StackedInline):
    model = models.Cart
    can_delete = False
    extra = 1


class FoeInline(admin.StackedInline):
    model = models.Foe
    can_delete = False
    extra = 1
    # raw_id_fields = ('consumer_type',)


@admin.register(models.Avatar)
class AvatarAdmin(HistoryBlockAdmin, admin.ModelAdmin):
    inlines = (CartInline, FoeInline)
    save_on_top = True
    list_display = ('avatar_name', 'is_bot', 'level', 'total_kill', 'total_money', 'in_game', 'created',)
    readonly_fields = ('avatar_id',)


@admin.register(models.Item)
class ItemAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('title', 'thumb', 'base_price', 'base_speed')


@admin.register(models.Enemy)
class EnemyAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('name', 'thumb', 'modified', 'enemy_type', 'base_life')


@admin.register(models.Log)
class LogAdmin(admin.ModelAdmin):
    """docstring for LogAdmin"""
    list_display = ('log', 'category', 'consumer', 'created')
    list_filter = ('category', )
    # formfield_overrides = {
    #     JSONField: {'form_class': ReadableJSONFormField},
    # }
    #readonly_fields = ('info',)


@admin.register(models.Achievement)
class AchievementAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('title', 'thumb', 'key', 'callback', 'bonus')


@admin.register(models.AvatarAchievement)
class AvatarAchievementAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('pk', 'avatar', 'consumer_pk', 'consumer_type', 'consumer')


@admin.register(models.Hero)
class HeroAdmin(admin.ModelAdmin):
    save_on_top = True
    list_display = ('name', 'thumb', 'modified')
