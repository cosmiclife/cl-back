import logging
import uuid
from django.utils import timezone
from decimal import Decimal

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Q
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.utils.html import mark_safe
from django.utils.formats import number_format

from apps.avatars import defaults
from apps.avatars.widgets import validate_svg

logger = logging.getLogger(__name__)
User = get_user_model()


class BaseConsumerAbstractModel(models.Model):
    """
    An abstract base class that any custom models probably should
    subclass.
    """
    # Content-object field
    consumer_type = models.ForeignKey(
        ContentType,
        verbose_name="Тип Контента",
        related_name="content_type_set_for_%(class)s",
        blank=True, null=True,
        on_delete=models.SET_NULL
    )
    consumer_pk = models.PositiveIntegerField("ID объекта")
    consumer = GenericForeignKey(
        ct_field="consumer_type", fk_field="consumer_pk")

    class Meta:
        abstract = True


class Avatar(models.Model):
    avatar_name = models.CharField(
        max_length=255, blank=True, null=True, verbose_name=u"Имя")
    account = models.ForeignKey(
        User,
        related_name="avatar",
        blank=True, null=True,
        verbose_name=u"Акаунт",
        on_delete=models.SET_NULL,
    )
    hero = models.ForeignKey(
        'Hero',
        related_name="hero",
        blank=True, null=True,
        verbose_name=u"Герой",
        on_delete=models.SET_NULL,
    )
    avatar_id = models.UUIDField(default=uuid.uuid4, verbose_name=u"Уникальный ID")
    modified = models.DateTimeField(
        auto_now=True, verbose_name=u"Изменен")
    created = models.DateTimeField(auto_now_add=True, verbose_name=u"Создан")
    is_bot = models.BooleanField(default=True, verbose_name="Бот?")
    level = models.PositiveIntegerField(default=0, verbose_name="Уровень")
    total_kill = models.PositiveIntegerField(
        default=0, verbose_name="Всего убито врагов")
    attack = models.DecimalField(
        max_digits=19, decimal_places=2, default=1.00, verbose_name="Урон за клик")
    total_money = models.DecimalField(
        max_digits=19, decimal_places=2, default=0.00, verbose_name="Итого заработанно")
    money = models.DecimalField(
        max_digits=19, decimal_places=2, default=0.00, verbose_name="Деньги")
    delta_click = models.DecimalField(
        max_digits=19, decimal_places=2, default=0.00, verbose_name="Средняя величина кликов в секунду")

    def __str__(self):
        return f"{self.avatar_name}"

    def get_attack_sec(self):
        attack_sec = 0
        for item in self.carts.all():
            attack_sec += item.quantity * item.consumer.base_speed
        return attack_sec

    def get_is_bot(self):
        bef = self.created + timezone.timedelta(seconds=+5)
        if bef > self.modified:
            self.is_bot = True
        else:
            self.is_bot = False
        self.save()

    def get_money(self):
        money = Decimal(15 + self.total_kill) * Decimal(pow(1.15, self.level))
        return money

    def get_money_humanize(self):
        orig = str(round(self.money, 2))
        return number_format(orig, force_grouping=True)

    def get_enemy_life(self, base_life, enemy_type=defaults.EC_ENEMY):
        if enemy_type == defaults.EC_ENEMY:
            delta = (base_life + self.total_kill) * Decimal(pow(1.15, self.level))
        else:
            delta = (base_life + self.total_kill) * Decimal(pow(1.15, self.level))
        return Decimal(delta)

    def get_attack(self):
        attack = 1 * Decimal(pow(1.15, self.level))
        return attack

    def in_game(self):
        return self.modified - self.created

    in_game.short_description = "В игре"

    def is_victory(self, life, click, modified):
        dt = 30 - Decimal(modified)
        attack = self.attack * click + self.get_attack_sec() * dt # 12 - кол-во кликов в секунду, это средняя возможная скорость нажатия без скриптов
        # dt = timezone.now() - modified
        logger.info(modified)
        logger.info("VICTORY")
        logger.info("Click: {}".format(click))
        logger.info("Total time: {}".format(dt))
        logger.info("Enemy Life: {}".format(str(life)))
        logger.info("Attack: {}".format(str(attack)))
        if dt > 30: # время не может быть больше 30 секунд
            return False
        if attack >= life:
            return True
        else:
            return False

    def get_achievement(self):
        achievements = [
            {
                'id': achievement.consumer.pk,
                'title': achievement.consumer.title,
                'description': achievement.consumer.description,
                'bonus': achievement.consumer.bonus
            } for achievement in self.achievements.select_related()]
        return achievements

    def get_new_achievement(self):
        exclude_ids = [a['consumer_pk'] for a in self.achievements.values('consumer_pk')]
        achieves = Achievement.objects.exclude(pk__in=exclude_ids).filter(
            (Q(callback__lte=self.level) & Q(key='level')) |
            (Q(callback__lte=self.total_money) & Q(key='total_money')) |
            (Q(callback__lte=self.total_kill) & Q(key='total_kill')) |
            (Q(callback__lte=self.carts.count()) & Q(key='item'))).values('pk', 'title', 'description', 'bonus')
        return [
            {
                'id': a['pk'],
                'title': a['title'],
                'description': a['description'],
                'bonus': a['bonus']
            } for a in achieves]

    def get_check_achievement(self, achive):
        for new_achieve in self.get_new_achievement():
            if new_achieve['id'] == achive['id']:
                return True
        return False

    def set_achievement(self, achive):
        self.money += Decimal(achive['bonus'])
        self.save()
        content_type = ContentType.objects.get_for_model(Achievement)
        AvatarAchievement.objects.create(consumer_pk=achive['id'], consumer_type=content_type, avatar=self)

    def get_logs(self):
        content_type = ContentType.objects.get_for_model(self)
        logs = Log.objects.filter(
            consumer_pk=self.pk, consumer_type=content_type
        ).order_by('-created')[:10]
        return logs

    def set_log(self, log, category=None):
        content_type = ContentType.objects.get_for_model(self)
        info = {
            'level': self.level,
            'total_kill': self.total_kill,
            'attack': str(int(self.attack)),
            'total_money': str(int(self.total_money)),
            'money': str(int(self.money)),
            'email': self.account.email if self.account else None,
            'items': [{
                'id': item.consumer_pk,
                'title': item.consumer.title,
                'quantity': item.quantity
            } for item in self.carts.select_related()]
        }
        return Log.objects.create(consumer_pk=self.pk, consumer_type=content_type,
                                  log=log, info=info, category=category)

    @classmethod
    def get_field_label(cls, field):
        try:
            label = cls._meta.get_field_by_name(field)[0].verbose_name
        except AttributeError:
            try:
                label = getattr(cls, field, ).__doc__
            except AttributeError:
                if field.endswith('_id'):
                    label = getattr(cls, field[:-3]).field.verbose_name
                else:
                    raise
        return f"{label}"

    @classmethod
    def get_exported_fields(cls):
        return [k for k in map(lambda field: field.name, cls._meta.fields)]

    class Meta:
        verbose_name = u"Аватар"
        verbose_name_plural = u"Аватары"
        ordering = ('-created',)


class Cart(BaseConsumerAbstractModel):
    avatar = models.ForeignKey(Avatar, related_name="carts", on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0, verbose_name="Колличество")
    modified = models.DateTimeField(
        auto_now=True, verbose_name=u"Изменен")

    def __str__(self):
        return f"{self.consumer}"

    def get_price(self, action='buy'):
        if action == 'buy':
            price = self.consumer.base_price * Decimal(pow(1.15, self.quantity))
        else:
            if self.quantity == 0:
                price = 0
            else:
                price = self.consumer.base_price * Decimal(pow(1.15, self.quantity - 1))
        return round(price, 2)

    def get_price_humanize(self, action='buy'):
        orig = str(self.get_price(action))
        return number_format(orig, force_grouping=True)

    class Meta:
        ordering = ("pk",)
        verbose_name = "Корзина"
        verbose_name_plural = "Корзина"


class Foe(BaseConsumerAbstractModel):
    avatar = models.ForeignKey(Avatar, related_name="foes", on_delete=models.CASCADE)
    life = models.DecimalField(
        max_digits=19, decimal_places=2, verbose_name="Кол-во жизней")
    money = models.DecimalField(
        max_digits=19, decimal_places=2, verbose_name="Кол-во денег")
    level = models.PositiveIntegerField(verbose_name="Уровень")
    modified = models.DateTimeField(
        auto_now=True, verbose_name=u"Изменен")
    created = models.DateTimeField(auto_now_add=True, verbose_name=u"Создан")
    is_killed = models.BooleanField(default=False, verbose_name="Убит?")

    def __str__(self):
        return f"({self.pk}) {self.consumer} - {self.consumer.get_enemy_type_display()}"

    class Meta:
        verbose_name = "Противник"
        verbose_name_plural = "Противники"


class Item(models.Model):
    title = models.CharField(max_length=255, verbose_name="Название")
    base_price = models.DecimalField(
        max_digits=19, decimal_places=2, verbose_name="Базовая цена")
    base_speed = models.DecimalField(
        max_digits=19, decimal_places=2, verbose_name="Базовая скорость прироста")
    image = models.FileField(verbose_name=u"Изображение", upload_to='items', blank=True, null=True, validators=[validate_svg])

    def __str__(self):
        return f"{self.title}"

    def thumb(self):
        if self.image:
            return mark_safe(f'<img src="{self.image.url}" width="100px" height="100px">')
        else:
            return mark_safe(
                '<img src="https://dummyimage.com/100x100/fafafa/363636.png" width="100px" height="100px">')

    thumb.allow_tags = True
    thumb.short_description = "Изображение"

    class Meta:
        ordering = ("pk",)
        verbose_name = "Товар"
        verbose_name_plural = "Товары"


class Enemy(models.Model):
    name = models.CharField(max_length=255, verbose_name="Имя")
    description = models.TextField(blank=True, default="", verbose_name="Описание")
    enemy_type = models.PositiveIntegerField("Тип врага",
                                             choices=defaults.ENEMY_CHOICES, default=defaults.EC_ENEMY)
    base_life = models.DecimalField(
        max_digits=9, decimal_places=2, verbose_name="Базовое кол-во жизней")
    modified = models.DateTimeField(
        auto_now=True, verbose_name=u"Изменен")
    image = models.FileField(verbose_name=u"Изображение", upload_to='enemys', blank=True, null=True, validators=[validate_svg])

    def __str__(self):
        return f"{self.name} - ({self.get_enemy_type_display()})"

    def thumb(self):
        if self.image:
            return mark_safe(f'<img src="{self.image.url}" width="100px" height="100px">')
        else:
            return mark_safe(
                '<img src="https://dummyimage.com/100x100/fafafa/363636.png" width="100px" height="100px">')

    thumb.allow_tags = True
    thumb.short_description = "Изображение"

    class Meta:
        ordering = ("pk",)
        verbose_name = "Враг"
        verbose_name_plural = "Враги"


class Log(BaseConsumerAbstractModel):
    """
    Класс для формирования журнала аватара:
    любые мелкие действия, события
    """
    log = models.TextField(verbose_name="Действия")
    info = models.JSONField(verbose_name="Информация", blank=True, null=True)
    category = models.CharField(verbose_name="Категория",
                                max_length=255, blank=True, null=True)
    created = models.DateTimeField(
        auto_now_add=True, verbose_name="Создан")

    def __str__(self):
        return f"{self.log}"

    class Meta:
        ordering = ("consumer_pk", "-created")
        verbose_name = "Действие"
        verbose_name_plural = "Действия"


class Achievement(models.Model):
    title = models.CharField(max_length=255, verbose_name="Название")
    description = models.TextField(blank=True, default="", verbose_name="Описание")
    image = models.FileField(verbose_name=u"Изображение", upload_to='achievements', blank=True, null=True, validators=[validate_svg])
    key = models.CharField(max_length=255, verbose_name="Ключ",
                           choices=defaults.ACHIEVEMENT_CHOICES, blank=True, null=True)
    callback = models.PositiveIntegerField(
        verbose_name="Условие получения",
        default=0
    )
    bonus = models.DecimalField(
        max_digits=19, decimal_places=2, verbose_name="Награда")
    created = models.DateTimeField(
        auto_now_add=True, verbose_name="Создан")
    modified = models.DateTimeField(
        auto_now=True, verbose_name=u"Изменен")

    def __str__(self):
        return f"{self.title}"

    def thumb(self):
        if self.image:
            return mark_safe(f'<img src="{self.image.url}" width="100px" height="100px">')
        else:
            return mark_safe('<img src="https://dummyimage.com/100x100/fafafa/363636.png" width="100px" height="100px">')

    thumb.allow_tags = True
    thumb.short_description = "Изображение"

    class Meta:
        ordering = ("pk",)
        verbose_name = "Достижение"
        verbose_name_plural = "Достижения"


class AvatarAchievement(BaseConsumerAbstractModel):
    avatar = models.ForeignKey(Avatar, related_name="achievements", on_delete=models.CASCADE)
    modified = models.DateTimeField(
        auto_now=True, verbose_name=u"Изменен")
    created = models.DateTimeField(auto_now_add=True, verbose_name=u"Создан")

    def __str__(self):
        return f"{self.consumer}"

    class Meta:
        verbose_name = "Достижение персонажа"
        verbose_name_plural = "Достижения персонажей"


class Hero(models.Model):
    name = models.CharField(max_length=255, verbose_name="Имя")
    base_life = models.DecimalField(
        max_digits=9, decimal_places=2, verbose_name="Базовое кол-во жизней")
    modified = models.DateTimeField(
        auto_now=True, verbose_name=u"Изменен")
    image = models.FileField(verbose_name=u"Изображение", upload_to='heroes', blank=True, null=True, validators=[validate_svg])

    def __str__(self):
        return f"{self.name}"

    def thumb(self):
        if self.image:
            return mark_safe(f'<img src="{self.image.url}" width="100px" height="100px">')
        else:
            return mark_safe('<img src="https://dummyimage.com/100x100/fafafa/363636.png" width="100px" height="100px">')

    thumb.allow_tags = True
    thumb.short_description = "Изображение"

    class Meta:
        ordering = ("pk",)
        verbose_name = "Герой"
        verbose_name_plural = "Герой"
