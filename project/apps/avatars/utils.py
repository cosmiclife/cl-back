import random
from . import models, defaults
from django.contrib.contenttypes.models import ContentType
from rest_framework.validators import UniqueValidator, ValidationError


def _get_enemy(avatar, bb=False):
    """
    Получение противника для боя, если уровень равен 10
    то выдается босс, или выдается список противников
    """
    pks_enemy = [en["pk"] for en in models.Enemy.objects.filter(
        enemy_type=defaults.EC_ENEMY).values("pk")]
    pks_enemy_boss = [en["pk"] for en in models.Enemy.objects.filter(
        enemy_type=defaults.EC_BOSS).values("pk")]
    if not avatar.foes.filter(consumer_pk__in=pks_enemy):
        create = True
        enemys = models.Enemy.objects.filter(enemy_type=defaults.EC_ENEMY)
        enemy = random.choice(enemys)
        foe = models.Foe()
        foe.avatar = avatar
        foe.life = avatar.get_enemy_life(enemy.base_life, enemy.enemy_type)
        foe.money = 0
        foe.level = 0
        foe.is_killed = True #нулевые мертвы
        foe.consumer_type = ContentType.objects.get_for_model(models.Enemy)
        foe.consumer_pk = enemy.pk
        foe.save()
    if not avatar.foes.filter(consumer_pk__in=pks_enemy_boss):
        enemys = models.Enemy.objects.filter(enemy_type=defaults.EC_BOSS)
        enemy = random.choice(enemys)
        foe = models.Foe()
        foe.avatar = avatar
        foe.life = avatar.get_enemy_life(enemy.base_life, enemy.enemy_type)
        foe.money = 0
        foe.level = 0
        foe.is_killed = True #нулевые мертвы
        foe.consumer_type = ContentType.objects.get_for_model(models.Enemy)
        foe.consumer_pk = enemy.pk
        foe.save()
    if avatar.foes.filter(is_killed=False):
        return avatar.foes.filter(is_killed=False)
    else:
        if not avatar.foes.filter(is_killed=True, consumer_pk__in=pks_enemy):
            return avatar.foes.filter(is_killed=False)
        foe = avatar.foes.filter(is_killed=True, consumer_pk__in=pks_enemy).first()
        if foe.level%9==0 and foe.level > 0:
            foe.level = 0
            foe.save()
            enemys = models.Enemy.objects.exclude(pk=foe.consumer_pk).filter(enemy_type=defaults.EC_BOSS)
            foe = avatar.foes.filter(
                is_killed=True, consumer_pk__in=pks_enemy_boss).first() #оживляем боса
        else:
            enemys = models.Enemy.objects.exclude(pk=foe.consumer_pk).filter(enemy_type=defaults.EC_ENEMY)
        try:
            enemy = random.choice(enemys)
        except ImportError as e:
            raise ValidationError("Отсутсвует список врагов, добавьте в админке")
        foe.consumer_pk = enemy.pk
        foe.life = avatar.get_enemy_life(enemy.base_life, enemy.enemy_type)
        foe.is_killed = False
        foe.save()
        """
        if len(avatar.foes.filter(is_killed=True, consumer_pk__in=pks_enemy))%9==0 and bb:
            avatar.foes.filter(is_killed=True).delete()
            enemys = models.Enemy.objects.filter(enemy_type=defaults.EC_BOSS)
        elif len(avatar.foes.filter(is_killed=True)) > 10:
            avatar.foes.filter(is_killed=True).delete()
        else:
            enemys = models.Enemy.objects.filter(enemy_type=defaults.EC_ENEMY)
        enemy = random.choice(enemys)
        foe = models.Foe()
        foe.avatar = avatar
        foe.life = avatar.get_enemy_life(enemy.enemy_type)
        foe.money = 10
        foe.level = avatar.level
        foe.consumer_type = ContentType.objects.get_for_model(models.Enemy)
        foe.consumer_pk = enemy.pk
        foe.save()
        """
        return avatar.foes.filter(is_killed=False)


def _get_items(avatar):
    carts_c_pks = [cart["consumer_pk"] for cart in avatar.carts.values("consumer_pk")]
    for item in models.Item.objects.all():
        if item.base_price - 500 <= avatar.total_money and item.pk not in carts_c_pks:
            cart = models.Cart()
            cart.avatar = avatar
            cart.consumer_type = ContentType.objects.get_for_model(models.Item)
            cart.consumer_pk = item.pk
            cart.save()
    return avatar.carts.all()
