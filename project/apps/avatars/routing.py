from django.urls import path, re_path
from channels.routing import ProtocolTypeRouter, URLRouter
from .consumers import GameConsumer

websockets = URLRouter([
    path(
        'game', GameConsumer.as_asgi(),
        name='game',
    ),
])
