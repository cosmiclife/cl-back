from django.apps import AppConfig
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save
from django.utils.translation import gettext_lazy as _
from fieldsignals import post_save_changed


class AvatarsConfig(AppConfig):
    name = 'apps.avatars'
    verbose_name = _('Игра')

