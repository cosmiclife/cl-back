import json
import logging
import xml.etree.cElementTree as et

from PIL import Image

from django.contrib import admin
from django.contrib.postgres.fields import JSONField
from django.forms import widgets
from django.core.exceptions import ValidationError


logger = logging.getLogger(__name__)


def validate_svg(f):
    f.seek(0)
    tag = None
    try:
        for event, el in et.iterparse(f, ('start',)):
            tag = el.tag
            break
    except et.ParseError as e:
        logger.info(str(e))

    if tag != '{http://www.w3.org/2000/svg}svg':
        try:
            image = Image.open(f)
            image.verify()
            tag = 'image'
        except Exception as e:
            logger.info(str(e))

    if tag is None:
        raise ValidationError('Uploaded file is not an image or SVG file.')

    f.seek(0)
    return f


class PrettyJSONWidget(widgets.Textarea):

    def format_value(self, value):
        try:
            value = json.dumps(json.loads(value), indent=2, sort_keys=True)
            # these lines will try to adjust size of TextArea to fit to content
            row_lengths = [len(r) for r in value.split('\n')]
            self.attrs['rows'] = min(max(len(row_lengths) + 2, 10), 30)
            self.attrs['cols'] = min(max(max(row_lengths) + 2, 40), 120)
            return value
        except Exception as e:
            logger.warning("Error while formatting JSON: {}".format(e))
            return super(PrettyJSONWidget, self).format_value(value)


class JsonAdmin(admin.ModelAdmin):
    formfield_overrides = {
        JSONField: {'widget': PrettyJSONWidget}
    }
