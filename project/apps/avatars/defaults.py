EC_ENEMY, EC_BOSS = range(1,3)
ENEMY_CHOICES = (
    (EC_ENEMY, "Враг"),
    (EC_BOSS, "Босс"),
)

ACHIEVEMENT_CHOICES = (
    ('email', 'Email'),
    ('level', 'Уровень'),
    ('total_kill', 'Побед'),
    ('total_money', 'Заработанно'),
    ('money', 'Деньги'),
    ('item', 'Вооружение'),
)