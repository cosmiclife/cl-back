from random import choice, randint

__version__ = '1.0.2'


def generate(locale=None):
    base = '_'
    third_part_chance = 4
    prefix_chance = 0# 3
    max_nickname_length = 30
    hash_length = 0 # 4 если 0 то не добавляем цифры в имена
    firsts = ['Огонь',
              'Красный',
              'Синий',
              'Вспышка',
              'Зомби',
              'Диско',
              'Ниндзя',
              'Полет',
              'Капитан',
              'Бессмертный',
              'Ледяной',
              'Вечный',
              'Святой',
              'Растущий',
              'Прекрасный',
              'Массивный',
              'Фальшивый',
              'Пламенный',
              'Большой',
              'Жирный',
              'Био',
              'Плохишь',
              'Сокрытый',
              'Обреченный',
              'Сломанный',
              'Мистический',
              'Глыба',
              'Теоретический',
              'Еритический',
              'Тихий',
              'Потный',
              'Горячий',
              'Забытый',
              'Сумасшедший',
              'Героичный',
              'Бесшумный',
              'Бодрый',
              'Двойной',
              'Одинокий',
              'Боевой',
              'Бородатый',
              'Злой',
              'Невидимый',
              'Темный',
              'Черный',
              'Фантом',
              'Стальной',
              'Брутальный',
              'Могущественный',
              'Всемогущий',
              'Атомный',
              'Ядерный',
              'Сладенький',
              'Десятилетний']
    seconds = [
                'Панда',
                'Гидроцикл',
                'Брокколи',
                'Утенок',
                'Губка',
                'Секундомер',
                'Коктейль',
                'Лапша',
                'Саксофон',
                'Пенал',
                'Чизкейк',
                'Желе',
                'Таракан',
                'Комета',
                'Ус',
                'Дед',
                'Рестлер',
                'Тайна',
                'Диктатор',
                'Ковер',
                'Завтрак',
                'Гигант',
                'Клоун',
                'Лама',
                'Пони',
                'Червь',
                'Абажур',
                'Пожарный',
                'Осьминог',
                'Замок',
                'Колбаса',
                'Призрак',
                'Ложка',
                'Пират',
                'Таксидермист',
                'Кот',
                'Самурай',
                'Чистка',
                'Череп',
                'Рыцарь',
                'Зверь',
                'Мастер',
                'Хищник',
                'Ребенок',
                'Мутант',
                'Вампир',
                'Робот',
                'Демон',
                'Куб',
                'Цилиндр']
    thirds = ['Смертельный', # Какой?
              'Адский',
              'Сильный',
              'Мудрый',
              'Далекий',
              'Болотный',
              'Космический']
    first = firsts[randint(0, len(firsts) - 1)]
    second = seconds[randint(0, len(seconds) - 1)]
    third = ''
    if randint(0, third_part_chance) == 0:
        third = thirds[randint(0, len(thirds) - 1)]
        if len(third + base + first + second) > max_nickname_length:
            third = ''

    if prefix_chance and randint(0, prefix_chance) == 0:
        prefixes =['The', 'The', 'The', 'DJ']
        prefix = prefixes[randint(0, len(prefixes))]
        if len(prefix + third + base + first + second) < max_nickname_length:
            first = prefix + first

    hash_code = ''
    if hash_length > 0:
        hash_code = base + str(randint(0, 16 ** hash_length))

    if hash_code:
        return str(first + second + third + hash_code)
    if third:
        return str(third + ' ' + first + ' ' + second)
    return str(first + ' ' + second)
