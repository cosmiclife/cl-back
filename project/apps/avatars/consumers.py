import json
import logging
from decimal import Decimal
from asgiref.sync import sync_to_async, async_to_sync

from channels.exceptions import DenyConnection, RequestAborted
from channels.generic.websocket import AsyncWebsocketConsumer, WebsocketConsumer

from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import AnonymousUser

from apps.avatars.models import Avatar
from apps.avatars import defaults
from apps.avatars import utils
from apps.api.v1.serializers.avatars import AvatarFullSerializer, RankingSerializer

logger = logging.getLogger(__name__)


class GameConsumer(AsyncWebsocketConsumer):
    def _get_avatar(self, avatar_id):
        return Avatar.objects.filter(avatar_id=avatar_id).first()

    def _get_ranking_data(self, data, many=False):
        return RankingSerializer(instance=data, many=many).data

    def _get_full_avatar(self, avatar):
        return AvatarFullSerializer(instance=avatar).data if avatar else {}

    @sync_to_async
    def avatar_me(self, event):
        logger.info("=== ME ===")
        logger.info(event)
        avatar_id = event['avatar_id']
        action_data = event.get('data')
        status = 'success'
        message = ''
        avatar = self._get_avatar(avatar_id)
        if not avatar:
            status = 'fail'
            message = 'avatar not found'
        data = {
            'status': status,
            'message': message,
            'avatar': self._get_full_avatar(avatar)
        }
        return data

    @sync_to_async
    def avatar_ranking(self, event):
        logger.info("=== RANK ===")
        logger.info(event)
        avatar_id = event['avatar_id']
        action_data = event.get('data')
        status = 'success'
        message = ''
        avatar = self._get_avatar(avatar_id)
        if not avatar:
            status = 'fail'
            message = 'avatar not found'
        avatars = Avatar.objects.select_related().order_by('-level')[:10]
        data = {
            'status': status,
            'message': message,
            'ranking': {
                'list': self._get_ranking_data(avatars, True),
                'me': self._get_ranking_data(avatar, False)
            },
            'avatar': self._get_full_avatar(avatar)
        }
        return data

    @sync_to_async
    def avatar_victory(self, event):
        logger.info("=== VICTORY ===")
        avatar_id = event['avatar_id']
        action_data = event.get('data')
        status = 'success'
        message = ''
        avatar = self._get_avatar(avatar_id)
        if not avatar:
            status = 'fail'
            message = 'avatar not found'
        # тут логика
        foe_pk = action_data.get('foe_id')
        timer = action_data.get('timer')
        click = action_data.get('click')
        if not foe_pk:
            self.send(text_data=json.dumps({'status': 'fail', 'message': 'not foe'}))
            return
        try:
            foe = avatar.foes.get(consumer_pk=foe_pk, is_killed=False)
        except Exception as e:
            # logger.info([f.consumer_pk for f in avatar.foes.all()])
            self.send(text_data=json.dumps({'status': 'fail', 'message': str(e)}))
            return
        if not avatar.is_victory(foe.life, click, timer):
            # foe.modified = datetime.datetime.now() # Делаем сброс времени модификации, что бы исключить отправку чуть позже
            # foe.save()
            status = 'fail'
            message = 'not victory'
        else:
            foe.life = 0
            foe.is_killed = True
            foe.level += 1
            foe.save()
            if foe.consumer.enemy_type == defaults.EC_BOSS:
                level = avatar.level + 1
                avatar.level = level
                avatar.attack = avatar.get_attack()
            avatar.money += avatar.get_money()
            avatar.total_money += avatar.get_money()
            avatar.total_kill += 1
            avatar.is_bot = False
            avatar.save()
            avatar.set_log("Победа над врагом - %s" % foe, "Сражение")
            utils._get_enemy(avatar)
            utils._get_items(avatar)
            status = 'success'
            message = 'victory'

        data = {
            'status': status,
            'message': message,
            'avatar': self._get_full_avatar(avatar)
        }
        return data

    @sync_to_async
    def avatar_shop(self, event):
        logger.info("=== SHOP ===")
        avatar_id = event['avatar_id']
        action_data = event.get('data')
        status = 'success'
        message = ''
        avatar = self._get_avatar(avatar_id)
        if not avatar:
            status = 'fail'
            message = 'avatar not found'
        # тут логика
        item_pk = action_data.get('item_id')
        act = action_data.get('act')
        if not item_pk or not act:
            status = 'fail'
            message = 'not item'
        cart = avatar.carts.get(pk=item_pk)
        if act == 'buy' and avatar.money >= cart.get_price('buy'):
            avatar.money -= Decimal(cart.get_price('buy'))
            avatar.save()
            cart.quantity += 1
            cart.save()
            avatar.set_log("Покупка оружия - %s" % cart, "Товары")
        elif act == 'sell' and cart.quantity > 0:
            avatar.money += Decimal(cart.get_price('sell'))
            avatar.save()
            cart.quantity -= 1
            cart.save()
            avatar.set_log("Продажа оружия - %s" % cart, "Товары")
        else:
            status = 'fail'
            message = 'not money'

        data = {
            'status': status,
            'message': message,
            'avatar': self._get_full_avatar(avatar)
        }
        return data

    @sync_to_async
    def avatar_achieve(self, event):
        logger.info("=== ACHIEVE ===")
        logger.info(event)
        avatar_id = event['avatar_id']
        action_data = event.get('data')
        status = 'success'
        message = ''
        avatar = self._get_avatar(avatar_id)
        if not avatar:
            status = 'fail'
            message = 'avatar not found'
        if avatar.get_check_achievement(action_data):
            avatar.set_achievement(action_data)
            message = 'achive check'
        data = {
            'status': status,
            'message': message,
            'avatar': self._get_full_avatar(avatar)
        }
        return data

    async def connect(self):
        self.room_group_name = 'game_{}'.format(self.channel_name.split('.')[1][9:20])

        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )
        await self.accept()

    actions = {
        'me': avatar_me,
        'victory': avatar_victory,
        'shop': avatar_shop,
        'achieve': avatar_achieve,
        'ranking': avatar_ranking
    }

    async def receive(self, text_data):
        data = json.loads(text_data)
        await self.channel_layer.group_send(
             self.room_group_name,
             {
                 'type': 'avatar_data',
                 'data': data
             }
        )

    async def avatar_data(self, event):
        data = event['data']
        logger.info('SEND {}'.format(event))
        response = await self.actions[data['action']](self, data)
        logger.info(response)
        await self.send(text_data=json.dumps(response, ensure_ascii=False, default=str))

    async def websocket_disconnect(self, message):
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )
